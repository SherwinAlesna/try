<?php
#########LATER
?>
<!doctype html>
<html>
<head>
	<title>Profile Picture</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<header>

		<div id = "whole">

			<div class = "signup">

				<form method = "POST" action = "../codes/upload_code.php" enctype = "multipart/form-data">

					<div class = "inputs">

						<input type = "file" name = "file">
						<input type = "submit" name = "btn_upload" value = "Confirm" class = "btn btn-primary btn-block btn-large">
						<a href="../index.php" class = "btn btn-primary btn-block btn-large">Skip this step</a>
					</div>

				</form>

			</div>

					<div class = "error">
						<?php 
							$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

							if(strpos($url, "error=type"))
							{
								echo "Image type is not allowed";
							}
							elseif(strpos($url, "error=err"))
							{
								echo "Something went wrong";
							}
							elseif(strpos($url, "error=size"))
							{
								echo "Image size is too big";
							}
						?>
					</div>
		</div>

	</header>
</body>
</html>