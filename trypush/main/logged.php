<?php
session_start();
	include("../codes/logged_code.php");
?>
<!doctype html>
<html>
<head>
	<title>Welcome <?php echo $guest['lastname']; ?></title>
	<link rel="stylesheet" type="text/css" href="../logged.css">
</head>
<body>
	<header>
		<div class = "logout">
			<a href="../codes/logout.php">Logout</a>
		</div>

		<div class = "edit">
			<a href="../main/edit.php">Edit Profile</a>
		</div>

		<div class = "img">
			<?php echo $profile; ?>
			<p><?php echo $guest['username']; ?></p>
		</div>

	</header>
</body>
</html>