<!doctype html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<header>

		<div id = "whole">

			<div class = "signup">

				<form method = "POST" action = "../codes/register_code.php">

					<div class = "inputs">

						<input type = "text" name = "first_name" placeholder = "First name" autofocus>
						<input type = "text" name = "last_name" placeholder = "Last name">
						<input type = "text" name = "username" placeholder = "Username">
						<input type = "password" name = "password" placeholder = "Password">
						<input type = "password" name = "password2" placeholder = "Confirm Password">
						<input type = "email" name = "email" placeholder = "Email">
						<input type = "submit" name = "btn_register" value = "Register" class = "btn btn-primary btn-block btn-large">
						<a href="../index.php" class = "btn btn-primary btn-block btn-large">Back</a>
					</div>

				</form>

			</div>

			<div class = "error">
				<p>
				<?php

					$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
					if(strpos($url, "error=exist"))
					{
						echo "User already Exists";
					}
					elseif(strpos($url, "error=pass"))
					{
						echo "Password did not match";
					}
					elseif(strpos($url, "success"))
					{
						echo "Successfully created";
					}
					elseif(strpos($url, "error=empty"))
					{
						echo "Input all fields";
					}
				?>
				</p>
			</div>

		</div>

	</header>
</body>
</html>