<!doctype html>
<html>
<head>
	<title>Edit</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<header>

		<div id = "whole">

			<div class = "signup">
				<h2>Picture for now</h2>
				<form method = "POST" action = "../codes/edit_code.php" enctype = "multipart/form-data">

					<div class = "inputs">

						<input type = "text" name = "e_first_name" placeholder = "First name" autofocus>
						<input type = "text" name = "e_last_name" placeholder = "Last name">
						<input type = "text" name = "e_username" placeholder = "Username">
						<input type = "password" name = "e_password" placeholder = "Password">
						<input type = "password" name = "e_password2" placeholder = "Confirm Password">
						<input type = "email" name = "e_email" placeholder = "Email">
						<input type = "file" name = "e_file">
						<input type = "submit" name = "btn_edit" value = "Confirm" class = "btn btn-primary btn-block btn-large">
						<a href="../main/logged.php" class = "btn btn-primary btn-block btn-large">Back</a>
					</div>

				</form>

			</div>

			<div class = "error">
				<p>
				<?php

					$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

						if(strpos($url, "error=e_type"))
						{
							echo "Image type is not allowed";
						}
						elseif(strpos($url, "error=e_err"))
						{
							echo "Something went wrong";
						}
						elseif(strpos($url, "error=e_size"))
						{
							echo "Image size is too big";
						}
						elseif(strpos($url, "updated"))
						{
							echo "Edit success";
						}
				?>
				</p>
			</div>

		</div>

	</header>
</body>
</html>