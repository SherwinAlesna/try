<!doctype html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header>

		<div id = "whole">

			<div class = "signup">

				<form method = "POST" action = "codes/login_code.php">

					<div class = "inputs">

						<input type = "text" name = "username" placeholder = "Username" autofocus>
						<input type = "password" name = "password" placeholder = "Password">
						<input type = "submit" name = "btn_login" value = "Login" class = "btn btn-primary btn-block btn-large">
						<a href="main/register.php" class = "btn btn-primary btn-block btn-large">Register</a>
					</div>

				</form>

			</div>

			<div class = "error">
				<p>
				<?php
					$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

					if(strpos($url, "error=wrong"))
					{
						echo "Wrong username or password";
					}
				?>
				</p>
			</div>

		</div>

	</header>
</body>
</html>